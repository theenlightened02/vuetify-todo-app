import Vue from 'vue'
import Vuex, { Store } from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    InitialId: 1,
    tasks: [
      {
        id: 1,
        title: "Wake up",
        done: false,
        dueDate: '2021-10-09'
      },
      {
        id: 2,
        title: "Get Bananas",
        done: false,
        dueDate: '2021-10-10'
      },
      {
        id: 3,
        title: "Eat Bananas",
        done: false,
        dueDate: null
      }
    ],
    snackbar: {
      show: false,
      text: ''
    }
  },
  mutations: {
    addTask(state, newTaskTitle) {
      let newTask = {
        id: state.InitialId,
        title: newTaskTitle,
        done: false,
        dueDate: null
      };
      if (newTask.title === "") {
        // alert("Cannot be empty");
        state.snackbar.text = 'Task not defined'
        // throw 'Task Cannot be empty!';
      } else {
        state.InitialId++;
        state.tasks.push(newTask);
      }
    },
    doneTask(state, id) {
      let task = state.tasks.filter((task) => task.id === id)[0];
      task.done = !task.done;
    },
    deleteTask(state, id) {
      state.tasks = state.tasks.filter((task) => task.id !== id);
    },
    showSnackbar(state, text) {
      let timeout = 0;

      if (state.snackbar.show) {
        state.snackbar.show = false;
        timeout = 300;
      }
      setTimeout(() => {
        state.snackbar.show = true;
        state.snackbar.text = text;
      }, timeout);

      state.snackbar.show = true;
      state.snackbar.text = text;
    },
    hideSnackbar(state) {
      state.snackbar.show = false;
    },
    updateTaskTitle(state, payload) {
      // console.log('payload: ', payload);
      let task = state.tasks.filter((task) => task.id === payload.id)[0];
      task.title = payload.title;
    },
    updateTaskDueDate(state, payload) {
      // console.log('payload: ', payload);
      let task = state.tasks.filter((task) => task.id === payload.id)[0];
      task.dueDate = payload.dueDate;
    }


  },
  actions: {
    addTask({ commit }, newTaskTitle) {
      commit('addTask', newTaskTitle);
      if (this.state.snackbar.text === 'Task not defined') {
        commit('showSnackbar', 'Task not defined!');
        throw 'Task not defined';
      }
      commit('showSnackbar', 'Task Added Successfully!');
    },
    deleteTask({ commit }, id) {
      commit('deleteTask', id);
      commit('showSnackbar', 'Task Deleted!');
    },
    updateTaskTitle({ commit }, payload) { 
      commit('updateTaskTitle', payload)
      commit('showSnackbar', 'Task Updated!')
    },
    updateTaskDueDate({ commit }, payload) { 
      commit('updateTaskDueDate', payload)
      commit('showSnackbar', 'Due date Updated!')
    },
    


  },
  getters: {
  }
})
